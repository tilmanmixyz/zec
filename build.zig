const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{
        .preferred_optimize_mode = .ReleaseSafe,
    });

    const jemalloc_opt = b.option(bool, "jemalloc", "Build a test using the jemalloc allocator") orelse false;

    const lib = b.addModule("zec", .{
        .root_source_file = .{ .path = "src/zec.zig" },
        .target = target,
        .optimize = optimize,
    });

    const test_exe = b.addExecutable(.{
        .name = "zec-test",
        .target = target,
        .optimize = optimize,
        .root_source_file = .{ .path = "src/main.zig" },
    });
    test_exe.root_module.addImport("zec", lib);
    b.installArtifact(test_exe);

    if (jemalloc_opt) {
        test_exe.linkLibC();
        test_exe.linkSystemLibrary("jemalloc");
    }
}
