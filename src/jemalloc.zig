const std = @import("std");
const zec = @import("zec");
const c = @cImport({
    @cInclude("jemalloc/jemalloc.h");
});

const Jemalloc = struct {
    const vtable = std.mem.Allocator.VTable{
        .alloc = alloc,
        .free = free,
        .resize = resize,
    };

    pub fn init() Jemalloc {
        return Jemalloc{};
    }

    pub fn allocator(_: *const Jemalloc) std.mem.Allocator {
        return std.mem.Allocator{
            .ptr = undefined,
            .vtable = &vtable,
        };
    }

    pub fn alloc(_: *anyopaque, len: usize, log2_align: u8, ra: usize) ?[*]u8 {
        _ = ra;
        _ = log2_align;
        std.debug.assert(len > 0);

        const align_len = std.mem.alignForward(usize, len, std.mem.page_size);
        const ptr = c.je_mallocx(align_len, c.MALLOCX_ALIGN(align_len));
        return @ptrCast(ptr);
    }

    pub fn free(_: *anyopaque, slice: []u8, log2_buf_align: u8, ra: usize) void {
        _ = log2_buf_align;
        _ = ra;
        c.je_free(slice.ptr);
    }

    pub fn resize(_: *anyopaque, buf_unaligned: []u8, log2_buf_align: u8, new_len: usize, ra: usize) bool {
        _ = ra;
        _ = log2_buf_align;

        std.debug.assert(new_len > 0);

        const buf_align_len = std.mem.alignForward(usize, buf_unaligned.len, std.mem.page_size);
        _ = c.je_rallocx(buf_unaligned.ptr, new_len, c.MALLOCX_ALIGN(buf_align_len));
        return true;
    }
};

pub fn main() !void {
    const jemalloc = Jemalloc.init();
    const alloc = jemalloc.allocator();

    var vec = try zec.Vec(usize).init(alloc, .{});
    defer vec.deinit();
    const start = std.time.milliTimestamp();
    for (0..100000) |i| {
        try vec.push(i);
    }
    const end = std.time.milliTimestamp();
    for (0..vec.getLen()) |index| {
        const item = try vec.get(index);
        std.debug.print("{}\n", .{item});
    }
    for (0..vec.getLen()) |_| {
        const item = try vec.pop();
        std.debug.print("{}\n", .{item});
    }
    // Out of bounds check for Issue #1
    var oobvec = try zec.Vec(usize).init(alloc, .{});
    for (0..100) |i| {
        try oobvec.push(i);
    }
    try oobvec.resize(oobvec.getCap() / 4);
    try oobvec.push(69420);
    std.debug.print("it took {} ms\n", .{start - end});
}
