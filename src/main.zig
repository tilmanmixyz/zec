const std = @import("std");
const zec = @import("zec");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var arena = std.heap.ArenaAllocator.init(gpa.allocator());
    const alloc = arena.allocator();
    var vec = try zec.Vec(usize).init(alloc, .{});
    defer vec.deinit();
    for (0..100) |i| {
        try vec.push(i);
    }
    for (0..vec.len()) |index| {
        const item = try vec.get(index);
        std.debug.print("{}\n", .{item});
    }
    for (0..vec.len()) |_| {
        const item = try vec.pop();
        std.debug.print("{}\n", .{item});
    }
    // Out of bounds check for Issue #1
    var oobvec = try zec.Vec(usize).init(alloc, .{});
    for (0..100) |i| {
        try oobvec.push(i);
    }
    try oobvec.resize(oobvec.cap() / 4);
    try oobvec.push(69420);
}
