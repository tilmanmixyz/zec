const std = @import("std");
const mem = std.mem;
const Allocator = mem.Allocator;

pub const Config = struct {
    cap: usize = 4,
};

pub fn Vec(comptime T: type) type {
    return struct {
        allocator: Allocator,
        _len: usize,
        _cap: usize,
        data: ?[]T = null,

        pub const Error = error{
            OutOfBounds,
            VecIsEmpty,
            Overflow,
        };

        pub fn init(allocator: Allocator, config: Config) !Vec(T) {
            const data = try allocator.alloc(T, config.cap);
            return Vec(T){
                ._len = 0,
                ._cap = try std.math.ceilPowerOfTwo(config.cap),
                .data = data,
                .allocator = allocator,
            };
        }

        pub inline fn push(self: *Vec(T), item: T) !void {
            if (self._len >= self._cap) {
                try self.grow(self._cap);
            }
            self.data.?[self._len] = item;
            self._len += 1;
        }

        pub inline fn get(self: *Vec(T), index: usize) Error!T {
            if (index >= self._len) {
                return Error.OutOfBounds;
            }
            return self.data.?[index];
        }

        pub inline fn pop(self: *Vec(T)) Error!T {
            if (self._len == 0) {
                return Error.VecIsEmpty;
            }
            self._len -= 1;
            return self.data.?[self._len];
        }

        pub inline fn resize(self: *Vec(T), new_cap: usize) !void {
            self._cap = new_cap;
            if (self.data) |data| {
                self.data = try self.allocator.realloc(data, self._cap);
            } else {
                self.data = try self.allocator.alloc(T, self._cap);
            }
            if (self._cap < self._len) {
                self._len = self._cap;
            }
        }

        pub inline fn grow(self: *Vec(T), add: usize) !void {
            try self.resize(self._cap + add);
        }

        pub fn deinit(self: *Vec(T)) void {
            if (self.data) |data| {
                self.allocator.free(data);
            }
        }

        pub inline fn len(self: *Vec(T)) usize {
            return self._len;
        }

        pub inline fn cap(self: *Vec(T)) usize {
            return self._cap;
        }
    };
}
